// Component Library Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Component Service Dependencies
import Constants from '../../services/Constants/Constants';

// Component File Dependencies
import './ListArray.css';
import List from '../List/List';

class ListArray extends Component {
  /* Lifecycle Methods
  *********************************************************************/
  constructor(props) {
    super(props);

    this.state = {
      listIndex: 0
    };
  }

  componentWillMount() {
    this.notifyParent = this.props.notifyParent.bind(this);
  }

  render() {
    return (
      <div className="ListArray">
        <div className="ListArray-item">
          <h2 className="ListArray-item_title">{Constants.LIST_KEYS.YESTERDAY.LABEL}</h2>
          <List notifyParent={this.notifyParent} list={this.props.lists[this.state.listIndex+Constants.LIST_KEYS.YESTERDAY.INDEX]} />
        </div>
        <div className="ListArray-item">
          <h2 className="ListArray-item_title">{Constants.LIST_KEYS.TODAY.LABEL}</h2>
          <List notifyParent={this.notifyParent} list={this.props.lists[this.state.listIndex+Constants.LIST_KEYS.TODAY.INDEX]} />
        </div>
        <div className="ListArray-item">
          <h2 className="ListArray-item_title">{Constants.LIST_KEYS.TOMORROW.LABEL}</h2>
          <List notifyParent={this.notifyParent} list={this.props.lists[this.state.listIndex]} />
        </div>
      </div>
      );
  }

}

ListArray.propTypes = {
  lists: PropTypes.array,
  notifyParent: PropTypes.func
}

export default ListArray;
