// Component Library Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Component Service Dependencies
import Images from '../../services/Images/Images';

// Component File Dependencies
import './WheneverDrawer.css';

class WheneverDrawer extends Component {
    /* Lifecycle Methods
    *********************************************************************/
    constructor(props) {
        super(props);

    }

    componentWillMount() {
        this.closeDrawer = this.props.closeDrawer.bind(this);
    }

    render() {
        return (
            <div className={"WheneverDrawer" + (this.props.isOpen ? ' active' : '')} onClick={this.closeDrawer}>
                
            </div>
        );
    }

    /* Component Methods
    *********************************************************************/

}

WheneverDrawer.propTypes = {
    closeDrawer: PropTypes.func
}

export default WheneverDrawer;
