// Component Library Dependencies
import React, { Component } from 'react';

// Component Service Dependencies
import Images from '../services/Images/Images';
import DataManager from '../services/DataManager/DataManager';

// Component File Dependencies
import './App.css';
import Clock from './Clock/Clock';
import ListArray from './ListArray/ListArray';
import TaskModal from './Modal/TaskModal/TaskModal';
import WheneverDrawer from './WheneverDrawer/WheneverDrawer';

class App extends Component {
  /* Lifecycle Methods
  *********************************************************************/
  constructor(props) {
    super(props);

    this.toggleModal = this.toggleModal.bind(this);
    this.toggleWheneverDrawer = this.toggleWheneverDrawer.bind(this);
    this.updateLists = this.updateLists.bind(this);

    this.state = {
      modalIsOpen: false,
      wheneverDrawerIsOpen: false,
      lists: []
    };
  }

  componentWillMount() {
    this.updateLists();
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <div className="App-buttons">
            <button className="App-buttons_new button" onClick={this.toggleModal}>{Images.addIcon()}</button>
            <button className="App-buttons_settings button">{Images.settingsIcon()}</button>
          </div>
          <div className="App-clock">
            <Clock />
          </div>
        </div>

        <div className="App-lists">
          <ListArray lists={this.state.lists} notifyParent={this.updateLists} />
        </div>

        <div className="App-credits">
          <div>Icons made by <a href="http://www.freepik.com" target="_blank" title="Freepik" rel="noopener noreferrer">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon" target="_blank" rel="noopener noreferrer">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank" rel="noopener noreferrer">CC 3.0 BY</a></div>
        </div>

        <div className="App-whenever">
          <WheneverDrawer closeDrawer={this.toggleWheneverDrawer} />
        </div>

        <TaskModal isOpen={this.state.modalIsOpen} closeModal={this.toggleModal} notifyParent={this.updateLists} />
      </div>
    );
  }

  /* Component Methods
  *********************************************************************/

  toggleModal() {
    /* @NOTE

        The more I understand React, the more I'm wondering if this isn't an anti-pattern
        of sorts. I'm still altering this.props directly, just not DIRECTLY directly, which
        may be the reason it avoids the warning you get when trying to alter this.props.

        The reason for this pattern was i found i didnt have to pass the mutated object up the
        chain to be acted upon by the controlling parent. this.prop is passed by the controlling
        parent, so mutating it locally and then telling the parent to just update using the same
        object saved me the overhead of instantiating, passing and accepting new objects.

    */
    this.setState({
      modalIsOpen: !this.state.modalIsOpen
    });
  }

  toggleWheneverDrawer() {
    this.setState({
      wheneverDrawerIsOpen: !this.state.wheneverDrawerIsOpen
    });
  }

  updateLists() {
    console.log('update lists');
    this.setState({
      lists: DataManager.getListSet()
    });
  }
}

export default App;
