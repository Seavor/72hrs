// Component Library Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Component File Dependencies
import './List.css';
import ListTask from './ListTask/ListTask';

class List extends Component {
    /* Lifecycle Methods
    *********************************************************************/
    constructor() {
        super();

        this.mapTasks = this.mapTasks.bind(this);
        this.noListDisplay = this.noListDisplay.bind(this);
    }

    componentWillMount() {
        this.notifyParent = this.props.notifyParent.bind(this);
    }

    render() {
        return (
            <div className="List height-fix">
                {!this.props.list ? this.noListDisplay() :
                    <ul className="List-tasks">
                        {this.props.list.tasks.map(this.mapTasks)}
                    </ul>
                }
            </div>
        );
    }

    /* Component Methods
    *********************************************************************/
    mapTasks(task) {
        return <ListTask task={task} key={task.id} notifyParent={this.notifyParent}></ListTask>;
    }

    noListDisplay() {
        return (
            <div className="List-noData">No list data</div>
        );
    }
}

List.propTypes = {
    list: PropTypes.object,
    notifyParent: PropTypes.func
}

export default List;
