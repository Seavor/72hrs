// Component Library Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Component Service Dependencies
import Images from '../../../services/Images/Images';
import DataManager from '../../../services/DataManager/DataManager';
import TaskModal from '../../Modal/TaskModal/TaskModal';

// Component File Dependencies
import './ListTask.css';

class ListTask extends Component {
    /* Lifecycle Methods
    *********************************************************************/
    constructor() {
    	super();

    	this.toggleModal = this.toggleModal.bind(this);
    	this.togglePersist = this.togglePersist.bind(this);
    	this.toggleComplete = this.toggleComplete.bind(this);

        this.state = {
            modalIsOpen: false
        };
    }

    componentWillMount() {
        this.notifyParent = this.props.notifyParent.bind(this);

        this.setState({
            task: this.props.task
        });
    }

    render() {
        return (
            <li className="ListTask">
	            <button className="ListTask-wrapper clearfix" onClick={this.toggleModal}>
	            	<div className="ListTask-text single-line-ellipsis">
		            	{this.state.task.text}
	            	</div>
	            	<div className="ListTask-actions">
	            		{/* @NOTE
                         
	            			React doesnt seem to like nested buttons
	            			Per https://developer.mozilla.org, this seems like valid HTML5 syntax
	            			https://developer.mozilla.org/en-US/docs/Web/HTML/Element/button
							https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Content_categories#Phrasing_content

                            Moving on..

                            Huh.. might have missed this line in the spec..
                            Phrasing content, but there must be no interactive content descendant.

                            Note: I wonder what the proper pattern is for descendant interactivity. Sometimes, you might have an element
                            that has a top level click functionality, as well as inner functionality, as i have here. Do you play
                            DOM games and absolute position elements over one another so they arent technically descendants? Is this just
                            such bad UI/UX that its been built into the HTML spec? I will have to research this further
	            		*/}
	            		<button onClick={this.togglePersist} className={"button " + (this.state.task.persist ? 'selected' : '')}>
	            			{Images.favoriteIcon()}
            			</button>
	            		<button onClick={this.toggleComplete} className={"button " + (this.state.task.complete ? 'selected' : '')}>
	            			{Images.completeIcon()}
            			</button>
	            	</div>
	            </button>

	           <TaskModal isOpen={this.state.modalIsOpen} closeModal={this.toggleModal} task={this.state.task} notifyParent={this.notifyParent}></TaskModal>
            </li>
        );
    }

    /* Component Methods
    *********************************************************************/
    toggleModal() {
	    this.setState({
	      	modalIsOpen: !this.state.modalIsOpen
	    });
    }

    togglePersist(e) {
    	e.stopPropagation();

    	this.setState({
            task: DataManager.togglePersist(this.state.task)
        });

    	this.notifyParent();
    }

    toggleComplete(e) {
    	e.stopPropagation();

        this.setState({
            task: DataManager.toggleComplete(this.state.task)
        });

    	this.notifyParent();
    }
}

ListTask.propTypes = {
    task: PropTypes.object,
    notifyParent: PropTypes.func
}

export default ListTask;
