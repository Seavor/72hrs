// Component Library Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Component Service Dependencies
import Constants from '../../../services/Constants/Constants';
import Validation from '../../../services/Validation/Validation';
import Images from '../../../services/Images/Images';
import DataManager from '../../../services/DataManager/DataManager';

// Component File Dependencies
import './TaskModal.css';
import Modal from '../../Modal/Modal';

class TaskModal extends Component {
    static DEFAULT_FORM_STATE = {
        list: Constants.LIST_KEYS.TODAY.INDEX,
        textWarning: '',
        isOpen: false
    };

    /* Lifecycle Methods
    *********************************************************************/
    constructor(props) {
        super(props);

        this.handleNewListItem = this.handleNewListItem.bind(this);
        this.onTextChange = this.onTextChange.bind(this);
        this.onListChange = this.onListChange.bind(this);
        this.onStatusChange = this.onStatusChange.bind(this);
        this.close = this.close.bind(this);

        this.state = {...TaskModal.DEFAULT_FORM_STATE};
    }

    componentWillMount() {
        this.closeModal = this.props.closeModal.bind(this);
        this.notifyParent = this.props.notifyParent.bind(this);

        let task = this.props.task;

        if (task) {
            let pos = DataManager.getTaskListPosition(task),
                list = pos.length ? pos[0] : TaskModal.DEFAULT_FORM_STATE.list

            this.setState({ 
                task: task,
                list: list
            });
        } else {
            this.setState({
                task: DataManager.newTaskObject()
            });
        }
    }

    render() {
        return (
            <Modal isOpen={this.props.isOpen} closeModal={this.close}>
                <div className="TaskModal">
                    <h6>{this.props.task ? 'Edit' : 'New'} Item</h6>
                    <form onSubmit={this.handleNewListItem}>
                        <div className="TaskModal-text">
                            <input type="text" placeholder="Watchya doing?" value={this.state.task.text} ref="text" maxLength="100" onChange={this.onTextChange}></input>
                            <p className="error">{this.state.textWarning}</p>
                        </div>

                        <div className="TaskModal-status">
                            <div className="TaskModal-status_wrapper">
                                <label className="TaskModal-status_persist checkbox">
                                    <input type="checkbox" onChange={this.onStatusChange}  value="persist"
                                        checked={this.state.task.persist === true} />
                                    <span>Carry?</span>{Images.favoriteIcon()}
                                </label>
                                <label className="TaskModal-status_complete checkbox">
                                    <input type="checkbox" onChange={this.onStatusChange}  value="complete"
                                        checked={this.state.task.complete === true} />
                                    <span>Complete?</span>{Images.completeIcon()}
                                </label>
                            </div>
                        </div>

                        <div className="TaskModal-list">
                            <div className="TaskModal-list_item radio">
                              <label>
                                <input type="radio" onChange={this.onListChange} value={Constants.LIST_KEYS.TODAY.INDEX}
                                    checked={this.state.list === Constants.LIST_KEYS.TODAY.INDEX}
                                />
                                <span>{Constants.LIST_KEYS.TODAY.LABEL}</span>
                              </label>
                            </div>
                            <div className="TaskModal-list_item radio">
                              <label>
                                <input type="radio" onChange={this.onListChange} value={Constants.LIST_KEYS.TOMORROW.INDEX}
                                    checked={this.state.list === Constants.LIST_KEYS.TOMORROW.INDEX}
                                />
                                <span>{Constants.LIST_KEYS.TOMORROW.LABEL}</span>
                              </label>
                            </div>
                            <div className="TaskModal-list_item radio">
                              <label>
                                <input type="radio" onChange={this.onListChange} value={Constants.LIST_KEYS.WHENEVER.INDEX}
                                    checked={this.state.list === Constants.LIST_KEYS.WHENEVER.INDEX}
                                />
                                <span>{Constants.LIST_KEYS.WHENEVER.LABEL}</span>
                              </label>
                            </div>
                        </div>



                        <button>Submit</button>
                    </form>
                </div>
            </Modal>
        );
    }

    /* Component Methods
    *********************************************************************/
    handleNewListItem(e) {
        e.preventDefault();

        let invalid = Validation.minLength(this.state.task.text);

        if (invalid) {
            this.setState({ textWarning: invalid });
        } else {
            let task = this.state.task;
            
            if (!this.props.task) {
                let newState = {...TaskModal.DEFAULT_FORM_STATE};
                newState.task = DataManager.newTaskObject();
                
                DataManager.addTaskToList(task, this.state.list);

                this.setState(newState);
            } else {
                let pos = DataManager.getTaskListPosition(task);
                    
                task.text = this.state.task.text;
                task.complete = this.state.task.complete || false;
                task.persist = !this.state.task.complete ? (this.state.task.persist || false) : false;

                if (pos[0] !== this.state.list) {
                    DataManager.moveTask(task, this.state.list);
                } else {
                    DataManager.updateTask(task);
                }
            }
            
            /* @Note 
                this.boundPropMethod()() is a really annoying syntax.. whats up?? Bad binding pattern?

                Yup, was following a bad example of binding it at the component property level.
                Binding inside componentWillMount seems to be the proper pattern, or at least lets
                me get rid of that disgusting ()() syntax.
            */
            this.close();
        }
    }

    onTextChange(e) {
        let task = this.state.task,
            text = e.target.value,
            invalid = Validation.maxLength(text.length);

        task.text = text;

        this.setState({
            task: task,
            textWarning: invalid
        });
    }

    onListChange(e) {
        let list = parseInt(e.target.value, 10);

        this.setState({ list: list });
    }

    onStatusChange(e) {
        let task = this.state.task,
            value = e.target.value;

        if (value === 'persist') {
            task.persist = !task.persist;
            task.complete = false;
        }

        if (value === 'complete') {
            task.complete = !task.complete;
            task.persist = false;
        }

        this.setState({ task: task });
    }

    close() {
        this.closeModal();
        this.notifyParent();
    }
}

TaskModal.propTypes = {
    isOpen: PropTypes.bool,
    task: PropTypes.object,
    closeModal: PropTypes.func,
    notifyParent: PropTypes.func
}

export default TaskModal;
