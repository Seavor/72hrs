import uuid from 'uuid';
import Moment from 'moment-timezone';

// Component Service Dependencies
import Constants from '../../services/Constants/Constants';
import Store from '../../services/Store/Store';

class DataManager {
    
    /*
     * @Function DataManager.newDataObject
     *
     * Creates a new data object
     *
     * {tomorrow} [INT] optional number of days ahead to offset the creation date
     *                   - Created for use with List->Tomorrow creation
     *
     * return {
     *   id: STR,        // Unqiue ID
     *   date: TIMESTAMP // Time created
     *   tz: STR         // Timezone created
     * }
     */
    static newDataObject(tomorrow) {
        return {
            id: uuid.v4(),
            date: Moment().add(tomorrow || 0, 'days').toISOString(),
            tz: Moment.tz.guess()
        }
    }
    
    /*
     * @Function DataManager.newListObject
     *
     * Creates a new List object
     *
     * {tomorrow} [INT] optional number of days ahead to offset the creation date
     *
     * return DataManager.newDataObject.extend({
     *   tasks: ARR       // Repo for tasks
     * })
     */
    static newListObject(tomorrow) {
        let list = this.newDataObject(tomorrow);

        list.tasks = [];

        return list;
    }
    
    /*
     * @Function DataManager.DataManager
     *
     * Creates a new Task object
     *
     * {config} [MAP] optional settings for new task object
     * {config.complete} [BOOL] presets DataManager.DataManager.complete
     * {config.persist} [BOOL] presets DataManager.DataManager.persist
     *
     * return DataManager.newDataObject.extend({
     *   task: STR      // Task content
     *   complete: BOOl // Flag for if task completed
     *   persist: BOOL  // Flag for if task should perist to next current day
     * })
     */
    static newTaskObject(config) {
        let task = this.newDataObject();

        config = config || {};

        task.text = config.text || '';
        task.complete = config.complete || false;
        // Only allow persist flag is complete not set
        task.persist = !config.complete ? (config.persist || false) : false;

        return task;
    }

    /*
     * @Function DataManager.isToday
     *
     * Returns boolean value for whether given timestamp is apart of today given DataManager.newDataObject.tz offset
     *
     * {timestamp} [TIMESTAMP] optional number of days ahead to offset the creation date
     *
     * return BOOL
     */
    static isToday(timestamp) {
        let today = Moment().startOf('day');

        return Moment.tz(Moment(timestamp), Moment.tz.guess()).isSame(today, 'd');
    }

    /*
     * @Function DataManager.getTaskList
     *
     * Get index position of List and Task
     *
     * {task} [OBJ] task to be found
     *
     * return [INT, INT]
     */
    static getTaskListPosition(task) {
        let store = new Store(),
            lists = store.get(Constants.STORE_KEYS.LISTS);

        for (let l in lists) {
            for (let t in lists[l].tasks) {
                if (lists[l].tasks[t].id === task.id) {
                    return [parseInt(l, 10), parseInt(t, 10)];
                }
            }
        }

        return [];
    }

    /*
     * @Function DataManager.moveTask
     *
     * Move task at given index from arr1 to end of arr2
     *
     * {index} [INT] Index of task to be moved
     * {list1}  [ARR] List to move task from
     * {list2}  [ARR] List to move task to
     *
     * return VOID
     */
    static moveTask(task, listIndex) {
        let store = new Store(),
            lists = store.get(Constants.STORE_KEYS.LISTS),
            pos = this.getTaskListPosition(task);

        lists[pos[0]].tasks.splice(pos[1], 1);
        lists[listIndex].tasks.push(task);

        this.setListSet(lists);

        return true;
    }

    /*
     * @Function DataManager.persistTasks
     *
     * Takes a list of tasks, and moves ones marked TRUE via DataManager.newTaskObject.persist flag
     *
     * {fromTasks} [ARRAY] List to move persisted tasks out of
     * {toTasks}  [ARRAY] List to move persisted tasks into
     *
     * return VOID
     */
    static persistTasks(fromTasks, toTasks) {
        for (let t in fromTasks) {
            if (fromTasks[t].persist) {
                toTasks.push(fromTasks[t]);
                fromTasks.splice(t, 1);
            }
        }
    }


    /*
     * @Function DataManager.moveUnpersistedTasks
     *
     * Takes a list of tasks, and moves ones marked FALSE via DataManager.newTaskObject.persist flag
     *
     * {fromTasks} [ARRAY] List to move persisted tasks out of
     * {toTasks}  [ARRAY] List to move persisted tasks into
     *
     * return VOID
     */
    static moveUnpersistedTasks(fromTasks, toTasks) {
        for (let t in fromTasks) {
            if (!fromTasks[t].persist) {
                toTasks.push(fromTasks[t]);
                fromTasks.splice(t, 1);
            }
        }
    }

    /*
     * @Function DataManager.persistTaskByIndexToToday
     *
     * Flag a task as persist and move to today's list
     *
     * {index}  [INT] Index of Task to be worked on
     * {list}   [ARR] List of given task index
     *
     * return BOOL
     */
    // static persistTaskByIndexToToday(index) {
        // let store = new Store(),
            // todayList = store.get(Constants.STORE_KEYS.LISTS)[Constants.LIST_KEYS.TODAY.INDEX];

        // this.togglePersist(list[index], true);
        // this.moveTask(index, todayList);
    // }

    /*
     * @Function DataManager.togglePersist
     *
     * Toggles the DataManager.newTaskObject.persist flag
     * Turns off DataManager.newTaskObject.complete if toggle results in TRUE
     *
     * {task}   [MAP] Task to be worked on
     * {force}  [BOOL] Force toggle on
     *
     * return BOOL
     */
    static togglePersist(task, force) {
        let originalTask = {...task};

        task.persist = !task.persist;

        if (task.persist) {
            task.complete = force || false;
        }

        if (originalTask.complete !== task.complete || originalTask.persist !== task.persist) {
            this.updateTask(task);
        }

        return task;
    }

    /*
     * @Function DataManager.toggleComplete
     *
     * Toggles the DataManager.newTaskObject.complete flag
     * Turns off DataManager.newTaskObject.persist if toggle results in TRUE
     *
     * {task}   [MAP] Task to be worked on
     * {force}  [BOOL] Force toggle on
     *
     * return BOOL
     */
    static toggleComplete(task, force) {
        let originalTask = {...task};

        task.complete = !task.complete;

        if (task.complete) {
            task.persist = force || false;
        }

        if (originalTask.complete !== task.complete || originalTask.persist !== task.persist) {
            this.updateTask(task);
        }

        return task;
    }

    static updateTask(task) {
        let lists = this.getListSet(),
            pos = this.getTaskListPosition(task);

        lists[pos[0]].tasks[pos[1]] = task;

        this.setListSet(lists);
    }

    /*
     * @Function DataManager.newListSet
     *
     * Create a default set of lists, one for each list type found in Constants.LIST_KEYS
     *
     * return [
     *   (x4) DataManager.newListObject 
     * ]
     */
    static newListSet() {
        let lists = [
            this.newListObject(),
            this.newListObject(),
            this.newListObject(),
            this.newListObject(),
            this.newListObject()
        ];

        this.setListSet(lists);

        return lists;
    }

    /*
     * @Function DataManager.advanceListSetOneDay
     *
     * Takes a set of lists and advances tomorrow to today, persisting items flagged via DataManager.newTaskObject.persist
     *
     * return [
     *   (x4) DataManager.newListObject 
     * ]
     */
    static advanceListSetOneDay(lists) {
        this.moveUnpersistedTasks(lists[Constants.LIST_KEYS.YESTERDAY.INDEX].tasks, lists[Constants.LIST_KEYS.HISTORY.INDEX].tasks);
        this.moveUnpersistedTasks(lists[Constants.LIST_KEYS.TODAY.INDEX].tasks, lists[Constants.LIST_KEYS.YESTERDAY.INDEX].tasks);
        this.moveUnpersistedTasks(lists[Constants.LIST_KEYS.TOMORROW.INDEX].tasks, lists[Constants.LIST_KEYS.TODAY.INDEX].tasks);
        this.moveUnpersistedTasks(lists[Constants.LIST_KEYS.WHENEVER.INDEX].tasks, lists[Constants.LIST_KEYS.TOMORROW.INDEX].tasks);

        this.setListSet(lists);

        return lists;
    }

    /*
     * @Function DataManager.getListSet
     *
     * Returns lists from Store, creates initial set if none exist
     *
     * {index}  [INT] Index of Task to be worked on
     * {list}   [ARR] List of given task index
     *
     * return BOOL
     */
    static getListSet() {
        let store = new Store(),

            lists = store.get(Constants.STORE_KEYS.LISTS);

        if (!lists || !lists.length) {
            lists = this.newListSet();
        }

        if (!this.isToday(lists[Constants.LIST_KEYS.TODAY.INDEX].date)) {
            lists = this.advanceListSetOneDay(lists);
        }

        return lists;
    }

    static setListSet(lists) {
        console.log('set list set');
        let store = new Store();

        if (!lists.length) {
            return false;
        }

        store.set(Constants.STORE_KEYS.LISTS, lists);

        return true;
    }

    static addTaskToList(task, i) {
        let lists = this.getListSet();

        lists[i].tasks.push(task)

        this.setListSet(lists);
    }
}


export default DataManager;