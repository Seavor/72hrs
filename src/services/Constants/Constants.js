class Constants {
	static LIST_KEYS = {
		TOMORROW: {
			INDEX: 0,
			LABEL: 'Tomorrow'
		},
		TODAY: {
			INDEX: 1,
			LABEL: 'Today'
		},
		YESTERDAY: {
			INDEX: 2,
			LABEL: 'Yesterday'
		},
		WHENEVER: {
			INDEX: 3,
			LABEL: 'Whenever'
		},
		HISTORY: {
			INDEX: 4,
			LABEL: 'Whenever'
		}
	}

	static STORE_KEYS = {
		LISTS: 'lists'
	}
}


export default Constants;